# 编码加密 

- [进制转换](https://bbs.125.la/forum.php?mod=viewthread&tid=14061352&extra=)   
---  
- A2W
- W2A
    - 普通的MultiByteToWideChar和WideCharToMultiByte的调用，最为准确。    
---    
- A2W_J
- W2A_J
    - 优点在于可以让系统自动分配内存,理论上来说会提高速度。   
---  
- A2W_NT 
- W2A_NT 
    - NT函数文档少，我也没怎么研究，从函数名上只允许Unicode和ANSI互转，不是很符合我的需求。 
---  

- 编码_ANSI到Unicode
- 编码_Unicode到ANSI
- 编码_ANSI到UTF8
- 编码_UTF8到ANSI
- 编码_Unicode到UTF8
- 编码_UTF8到Unicode
    - 写了常用的几个，和Unicode互转的都提供两种模式。优化了精易ec的UTF-8转码默认有尾部\0容易造成校验出错，代码可读性和可维护性上更是秒杀精易ec。
    - 实际上其它的转码也相当容易，改一下CodePage即可，考虑有空写一个专门的贴子介绍一下我所理解的所有字符编码。

---  
- 编码_简体到繁体
- 编码_繁体到简体
    - 精易ec的这两条命令简化得谁也不认识，翻文档还原了一下。

---  

- 编码_ANSI到UCS2
- 编码_UCS2到ANSI
- 编码_Unicode到UCS2
- 编码_UCS2到Unicode 
    - 自己重写了一波，解决一些极限情况下的BUG，新增比较实用的Unicode和USC2互转。 
    - **注意：USC2 更名 UCS2，因为前者是个错误的词汇。**

---  
- 编码_URL解码   

    - 重写了  


- 编码_URL编码  

    - URL编码有很多种规范，我主要翻阅了JS中的，也就是
        - encodeURIComponent  
        - encodeURI  
        - escape
    这三种，并且提供字节集返回，这样对某些无法用ANSI装载的Unicode字符支持更好，demo代码中举了个问答区的新鲜的例子。  

---  
- 编码_BASE64编码  

    - 阅读文档写了个编码，速度还可以，比精易ec快数倍到无数倍(文本越长精易ec的速度越不能看，我都不能理解那种代码是什么样的人写出来的，而且留在里面那么多年)。    


- 编码_BASE64解码   

    - 普通的解码，优势是可以自定义Map。   
    - 抠来的辣鸡代码全部删掉,重写了。 


- ~~编码_BASE64解码_table~~  

    - 写了个查表法的 Base64 解码，速度快很多。   
    - 发现 BUG，弃用。
    - 重新写了个巨型查表法的 Base64 解码（128k 的 table），想学习的见 LookupTable 类，如果不是易的位运算性能拖后腿，将会相当地快速（已经很快了，比 API 慢一些）。

---

- 编码_BASE64解码_api 
- 编码_BASE64编码_api 
    - API 方式，稳定高速。   
    - 修复 XP 上的 BUG。

---

- [HTMLEntities - HTML 实体字符反转义](https://bbs.125.la/forum.php?mod=viewthread&tid=14088175&extra=)   

---  