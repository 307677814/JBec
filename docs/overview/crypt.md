# 哈希加密 

- [CryptoAPI类](http://bbs.125.la/forum.php?mod=viewthread&tid=14096085&extra=)  
    - 哈希算法   
        MD2/MD4/MD5/SHA1/SHA256/SHA384/SHA512   
        Hmac-MD5/Hmac-SHA1/Hmac-SHA256/Hmac-SHA384/Hmac-SHA512  
        
    - 加密算法   
        - AES-128  
        - AES-192  
        - AES-256  
        - DES  
        - 3DES(DESede)  
        - RC4  
        - RSA_public_encrypt
        - RSA_private_decrypt
        - RSA_sign
        - RSA_verify  

    
 - 填充类型 
        - NoPadding  
        - PKCS5Padding  
        - PKCS7Padding  
        - ZeroPadding  

- KeyFactory  

    - 尽量还原了 CryptoJS 3.1.2 对 key 和 IV 的自动填充，AES 除外，AES 的密钥补齐大概是 CryptoJS 的 BUG。     


---

- 类_MD5  

    - 修复了 XP 上的 BUG。  

- 类_SHA1  

    - 修复了 XP 上的 BUG。  

- 类_CRC32  
    - 大文件取哈希值专用，NT 函数，写了回调。 

---