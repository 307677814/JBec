# 组件  

- 类_脚本组件 
    - 封装了ScriptControl的所有属性和方法。 
    - 有一个[深坑](https://bbs.125.la/forum.php?mod=viewthread&tid=14093362&extra=)。

- 类_拖放对象  
    - 检测Windows版本，解决NT6.x以管理员身份运行时无法拖放，支持宽字符。
    - 修复一个 zz 的 BUG，增加坐标获取。 

- 类_剪辑板  
    - 简单封装，等待完善。

- 类_文件读写 
    - 主要大文件支持，写了一点点，太多了，懒得继续。

---

- [类_拨号](https://bbs.125.la/forum.php?mod=viewthread&tid=14096083&extra=) 
    - 这个是[深坑](https://bbs.125.la/forum.php?mod=viewthread&tid=14093362&extra=)，这个版本自己拿去改改用吧。

---

- 类_通用对话框 
    - WIN7 风格的通用对话框，我觉得最实用的就是可以自己输入路径，以及多选。

- 类_命令提示符  
    - 在别人的思路上修改成 系统_取DOS执行结果() 的替代品。然后：
    - [啧](https://bbs.125.la/forum.php?mod=viewthread&tid=14084312&extra=)  

