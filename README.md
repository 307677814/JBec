# ![JBec](./image/ico.ico)  [JBec](https://git.oschina.net/junkboy/JBec)

[![license](https://img.shields.io/badge/license-BSD3-blue.svg)](./LICENSE)
[![Release version](https://img.shields.io/badge/release-v1.0.0-blue.svg)](./release)

&#8195;&#8195;学习E已有一年半载，**JBec**初衷是一种总结。**JBec**将陆续包括**编码加密**、**文本操作**、**文件读写**、**算术运算**、**网络通信**等一些**我常用的**代码，并试图在流传广泛的ec和命令上进行一些**优化**。

&#8195;&#8195;**JBec**将只使用**krnln**（5.3#64版）和**win32api**为核心，不使用其它支持库，若干核心库命令似乎5.4.1和低版本不兼容,会尽量避开。

&#8195;&#8195;欢迎有兴趣的朋友贡献代码，提交Issues，在此之前请先查阅：[**JBec提issue的正确姿势**](./CONTRIBUTING.md)。

## 概览 

* [无锁日志](./docs/overview/JBLog.md) 
* [编码进制](./docs/overview/enc.md) 
* [窗口组件](./docs/overview/ui.md)  
* [哈希加密](./docs/overview/crypt.md) 
* [线程进程](./docs/overview/thread.md) 
* [文本操作](./docs/overview/char.md) 
* [文件目录](./docs/overview/file.md)  
* [算术运算](./docs/overview/math.md)  
* [网络通信](./docs/overview/socket.md)  

## 其他资源

* [clr皮肤](./clr/VS2015.clr)
* [字体](./font/inziu-iosevkaCC-slab-SC-regular.ttf)